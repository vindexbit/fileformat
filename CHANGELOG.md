# Changelog

---

## [0.4.2] - 2024-07-18

- Compilation flags related to libamalthea have been fixed.

---

## [0.4.1] - 2024-06-18

- Fixed code style.
- Fixed man- and help-pages.
- Other minor changes.

---

## [0.4.0] - 2022-09-25

- Added the "--formats" option that expand paths to find files
  with format descriptions.
- The "-m" or "--mime" flag adds output of MIME information,
  but doesn't disable output of format anymore.
- Improved output view.
- The project switched to dual licensing with BSL-1.0 and GNU GPL-3.0+.

---

## [0.3.2] - 2021-10-07

- Major version of Amalthea fixed.

---

## [0.3.1] - 2021-10-07

- Increased requirements for the version of the Amalthea library.
- Improved support for use with dub.

---

## [0.3.0] - 2021-07-30

- Building with dub is supported.
- Added MIME type detection with the '-m | --mime' option.
- More than one file can be passed as arguments.

---

## [0.2.1] - 2021-01-28

- Build system improved.
- README.md updated.
- Small changes in source code.

---

## [0.2] - 2020-02-11

- Minor improvements of messages.
- Removed package build scripts.

---

## [0.1] - 2019-07-17

- Initial release.

---
