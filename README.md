"fileformat" is free software.

This program is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

---

## fileformat

fileformat is a tool for recognizing file formats.

**Official repositories**:

- [https://codeberg.org/os-18/fileformat](https://codeberg.org/os-18/fileformat)
- [https://gitlab.com/os-18/fileformat](https://gitlab.com/os-18/fileformat)

---

## Ready-made packages

See:  
[Download Page of OS-18](https://codeberg.org/os-18/os-18-docs/src/branch/master/OS-18_Packages.md)  

The page contains information about installing several packages, including `fileformat`.

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for D (now this project supports compiler [ldc2](https://github.com/ldc-developers/ldc/)).

This project is assembled with a dynamic or static linking to [the Amalthea library](https://gitlab.com/os-18/amalthea). So if you want to build this project from source, you also need to build and install Amalthea. Then return to this instruction.


### Compilation and installation

Creating of executable bin-file:
```
$ make
```
Installation (by default, main directory is /usr/local/):
```
# make install
```

After that, the application is ready for use.

You can install this application in any other directory:
```
$ make install DESTDIR=home/$USER/sandbox PREFIX=usr
```
Uninstall:
```
# make uninstall
```

If you installed it in an alternate directory:
```
$ make uninstall DESTDIR=home/$USER/sandbox PREFIX=usr
```
---

## Usage

```
fileformat <file> ... [-m|--mime] [--formats <directory>]
    Show format name, format description and format type
    (audio, video, archives, etc.) for specified file(s).
    If '-m' or '--mime' has been used, the program will show
    the MIME type and its description also.
    The '--formats' option specifies a directory that stores JSON files
    with descriptions of custom file formats.
```

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

---
